// require('dotenv').config();
const TELEGRAM_API = require("node-telegram-bot-api");
const {
  managerActions,
  deliverActions,
  storekeeperActions,
  timeInterval,
} = require("./menu-options");
const TELEGRAM_TOKEN = "5565569405:AAG4VG_oL29_LbJKeltCGDMB0JzaB5_MhnE";
const {
  emailQuery,
  processingProductQuery,
  transportingProductQuery,
  countStatusesQuery,
} = require("./queryList");
let pool = require("./mysqlOptions/mysql").mysql__pool;
const mailer = require("./mailInstance/mail");

const BOT = new TELEGRAM_API(TELEGRAM_TOKEN, { polling: true });

async function changeProductStatus(chat_id, product) {
  console.log("product: ", product);

  if (product.post_status === "wc-processing") {
    await BOT.sendMessage(
      chat_id,
      `Заказ: ${product.ID}
Статус: Обработка
Клиент: ${product.first_name} ${product.last_name}
Телефон: ${product.phone}
Адрес: ${product.address}
Время и дата: ${product.date} ${product.time}
Товары: ${product.order_name}`,
      {
        reply_markup: JSON.stringify({
          resize_keyboard: true,
          inline_keyboard: [
            [
              { text: "Изменить", callback_data: "change" },
              { text: "Отмена", callback_data: "cancel" },
            ],
          ],
        }),
      }
    );
  }

  if (product.post_status === "wc-assembly") {
    await BOT.sendMessage(
      chat_id,
      `Заказ: ${product.ID}
Статус: Сборка
Клиент: ${product.first_name} ${product.last_name}
Телефон: ${product.phone}
Адрес: ${product.address}
Время и дата: ${product.date} ${product.time}
Товары: ${product.order_name}`,
      {
        reply_markup: JSON.stringify({
          resize_keyboard: true,
          inline_keyboard: [
            [
              { text: "Готов к доставке", callback_data: "change" },
              { text: "Нет в наличии", callback_data: "cancel" },
            ],
          ],
        }),
      }
    );
  }

  if (product.post_status === "wc-transporting") {
    await BOT.sendMessage(
      chat_id,
      `Заказ: ${product.ID}
Статус: Транспортировка
Клиент: ${product.first_name} ${product.last_name}
Телефон: ${product.phone}
Адрес: ${product.address}
Время и дата: ${product.date} ${product.time}
Товары: ${product.order_name}`,
      {
        reply_markup: JSON.stringify({
          resize_keyboard: true,
          inline_keyboard: [
            [
              { text: "Выполнен", callback_data: "transporting" },
              { text: "Не удался", callback_data: "failed" },
            ],
          ],
        }),
      }
    );
  }

  await BOT.once("callback_query", async (msg) => {
    if (msg.data === "change") {
      if (product.post_status === "wc-processing") {
        try {
          await pool
            .query(
              `UPDATE wp_posts SET post_status = 'wc-assembly' WHERE ID = ${product.ID}`
            )
            .then((response) => {
              setTimeout(() => {
                BOT.sendMessage(
                  chat_id,
                  `
Заказ: ${product.ID}
Статус изменён: Сборка
  
Заказ передан кладовщику
                `
                );
              }, 1000);
              console.log("update response: ", response);
            })
            .catch((pool_error) => {
              console.log(`pool_error: ${pool_error}`);
            });
        } catch (error) {
          console.log(`Something went wrong ${error}`);
        }
      }

      if (product.post_status === "wc-assembly") {
        try {
          await pool
            .query(
              `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${product.ID}`
            )
            .then((response) => {
              setTimeout(() => {
                BOT.sendMessage(
                  chat_id,
                  `
Заказ: ${product.ID}
Статус изменён: Транспортировка
  
Заказ передан курьеру
                `
                );
              }, 1000);
              console.log("update response: ", response);
            })
            .catch((pool_error) => {
              console.log(`pool_error: ${pool_error}`);
            });
        } catch (error) {
          console.log(`Something went wrong ${error}`);
        }
      }
    }

    if (msg.data === "transporting") {
      if (product.post_status === "wc-transporting") {
        try {
          pool
            .query(
              `UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${product.ID}`
            )
            .then((response) => {
              console.log("response: ", response);
            })
            .catch((pool_error) => {
              console.log("pool_error: ", pool_error);
            });

          setTimeout(() => {
            BOT.sendMessage(
              chat_id,
              `
Заказ: ${product.ID}

Статус изменён: Выполнен
              `
            );
          }, 1000);
        } catch (error) {
          console.log("error: ", error);
        }
      }
    }

    if (msg.data === "failed") {
      await BOT.sendMessage(chat_id, "Укажите причину");

      await BOT.once("message", async (msg) => {
        let text = msg.text;

        await setTimeout(() => {
          pool
            .query(
              `INSERT INTO wp_comments (comment_post_ID, comment_author, comment_content, comment_type) VALUE (${product.ID}, 'курьер', '${text}', 'order_note')`
            )
            .then((response) => {
              console.log("response: ", response);
            })
            .catch((pool_error) => {
              console.log("pool_error: ", pool_error);
            });

          BOT.sendMessage(chat_id, `Сообщение доставлено`);
        }, 1000);
      });
    }

    if (msg.data === "cancel") {
      try {
        const message = {
          to: `${product.email}`,
          subject: "Клиент успешно отменил свою заявочку",
          text: `Тут какой-то текст на html`,
        };

        if (message) {
          mailer(message);

          setTimeout(() => {
            BOT.sendMessage(chat_id, `Сообщение отправлено на почту`);
          }, 1000);
        }
      } catch (message_error) {
        console.log(`message error: ${message_error}`);
      }
    }
  });
}

async function chooseProductForTransporting(chat_id, arguments) {
  await setTimeout(() => {
    for (let i = 0; i < arguments.length; i++) {
      if (
        arguments[i].ID !== undefined &&
        arguments[i].first_name !== undefined &&
        arguments[i].last_name !== undefined &&
        arguments[i].phone !== undefined &&
        arguments[i].address !== undefined &&
        arguments[i].date !== undefined &&
        arguments[i].time !== undefined &&
        arguments[i].order_name !== undefined
      ) {
        BOT.sendMessage(
          chat_id,
          `
Заказ: ${arguments[i].ID}
Статус: Транспортировка
Клиент: ${arguments[i].first_name} ${arguments[i].last_name}
Телефон: ${arguments[i].phone}
Адрес: ${arguments[i].address}
Время и дата: ${arguments[i].date} ${arguments[i].time}
Товары: ${arguments[i].order_name}
      `,
          {
            reply_markup: JSON.stringify({
              inline_keyboard: [
                [
                  { text: "Транспортировка", callback_data: `${i}` },
                  { text: "Отмена", callback_data: "cancel" },
                ],
              ],
            }),
          }
        );
      }
    }
  }, 1000);

  await BOT.once("callback_query", async (msg) => {
    for (let i = 0; i < arguments.length; i++) {
      if (arguments[i].ID !== undefined && i === +msg.data) {
        changeProductStatus(chat_id, arguments[i]);
      }
    }

    if (msg.data === "cancel") {
      try {
        const message = {
          to: `${arguments[0].email}`,
          subject: "Клиент успешно отменил свою заявочку",
          text: `Тут какой-то текст на html`,
        };

        if (message) {
          mailer(message);

          setTimeout(() => {
            BOT.sendMessage(chat_id, `Сообщение отправлено на почту`);
          }, 1000);
        }
      } catch (message_error) {
        console.log(`message error: ${message_error}`);
      }
    }
  });
}

async function chooseProductForAssembly(chat_id, arguments) {
  await setTimeout(() => {
    for (let i = 0; i < arguments.length; i++) {
      if (
        arguments[i].ID !== undefined &&
        arguments[i].first_name !== undefined &&
        arguments[i].last_name !== undefined &&
        arguments[i].phone !== undefined &&
        arguments[i].address !== undefined &&
        arguments[i].date !== undefined &&
        arguments[i].time !== undefined &&
        arguments[i].order_name !== undefined
      ) {
        BOT.sendMessage(
          chat_id,
          `
Заказ: ${arguments[i].ID}
Статус: Сборка
Клиент: ${arguments[i].first_name} ${arguments[i].last_name}
Телефон: ${arguments[i].phone}
Адрес: ${arguments[i].address}
Время и дата: ${arguments[i].date} ${arguments[i].time}
Товары: ${arguments[i].order_name}
      `,
          {
            reply_markup: JSON.stringify({
              inline_keyboard: [
                [
                  { text: "Изменить статус", callback_data: `${i}` },
                  { text: "Отмена", callback_data: "cancel" },
                ],
              ],
            }),
          }
        );
      }
    }
  }, 1000);

  await BOT.once("callback_query", async (msg) => {
    console.log("msg: ", msg);
    for (let i = 0; i < arguments.length; i++) {
      if (arguments[i].ID !== undefined && i === +msg.data) {
        changeProductStatus(chat_id, arguments[i]);
      }
    }

    if (msg.data === "cancel") {
      try {
        const message = {
          to: `${arguments[0].email}`,
          subject: "Клиент успешно отменил свою заявочку",
          text: `Тут какой-то текст на html`,
        };

        if (message) {
          mailer(message);

          setTimeout(() => {
            BOT.sendMessage(chat_id, `Сообщение отправлено на почту`);
          }, 1000);
        }
      } catch (message_error) {
        console.log(`message error: ${message_error}`);
      }
    }
  });
}

async function chooseProductForManager(chat_id, arguments) {
  await setTimeout(() => {
    for (let i = 0; i < arguments.length; i++) {
      if (
        arguments[i].ID !== undefined &&
        arguments[i].first_name !== undefined &&
        arguments[i].last_name !== undefined &&
        arguments[i].phone !== undefined &&
        arguments[i].address !== undefined &&
        arguments[i].date !== undefined &&
        arguments[i].time !== undefined &&
        arguments[i].order_name !== undefined
      ) {
        BOT.sendMessage(
          chat_id,
          `
Заказ: ${arguments[i].ID}
Статус: Обработка
Клиент: ${arguments[i].first_name} ${arguments[i].last_name}
Телефон: ${arguments[i].phone}
Адрес: ${arguments[i].address}
Время и дата: ${arguments[i].date} ${arguments[i].time}
Товары: ${arguments[i].order_name}
      `,
          {
            reply_markup: JSON.stringify({
              inline_keyboard: [
                [
                  { text: "Сборка", callback_data: `${i}` },
                  { text: "Отмена", callback_data: "cancel" },
                ],
              ],
            }),
          }
        );
      }
    }
  }, 1000);

  await BOT.once("callback_query", async (msg) => {
    console.log("msg: ", msg);
    for (let i = 0; i < arguments.length; i++) {
      if (arguments[i].ID !== undefined && i === +msg.data) {
        changeProductStatus(chat_id, arguments[i]);
      }
    }

    if (msg.data === "cancel") {
      console.log("arguments: ", arguments[0].email);
      try {
        const message = {
          to: `${arguments[0].email}`,
          subject: "Клиент успешно отменил свою заявочку",
          text: `Тут какой-то текст на html`,
        };

        if (message) {
          mailer(message);

          setTimeout(() => {
            BOT.sendMessage(chat_id, `Сообщение отправлено на почту`);
          }, 1000);
        }
      } catch (message_error) {
        console.log(`message error: ${message_error}`);
      }
    }
  });
}

async function getTransportingProduct(chat_id) {
  let transportingCollection = [];

  try {
    await pool
      .query(transportingProductQuery)
      .then((response) => {
        console.log("response: ", response[0]);
        if (response[0].length === 0) {
          BOT.sendMessage(chat_id, "Заказов нет");
        }

        if (response[0].length > 0) {
          return response[0].forEach((elements) => {
            console.log("elements: ", elements);
            transportingCollection.push(elements);
          });
        }
      })
      .catch((pool_error) => {
        console.log(`pool error is ${pool_error}`);
      });
  } catch (error) {
    console.log(`Something went wrong ${error}`);
  }

  await chooseProductForTransporting(chat_id, transportingCollection);
}

async function getAssemblyProduct(chat_id) {
  let assemblyCollectionFrom10To12 = [];
  let assemblyCollectionFrom12To14 = [];
  let assemblyCollectionFrom14To16 = [];
  let assemblyCollectionFrom16To18 = [];
  let assemblyCollectionFrom18To20 = [];
  let assemblyCollectionFrom20To22 = [];

  await BOT.sendMessage(chat_id, "Выберете временной интервал", timeInterval);

  await BOT.once("callback_query", async (msg) => {
    if (msg.data === "1") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '10:00' AND '12:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);

            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom10To12.push(elements);
                }
              });
            }
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom10To12);
    }

    if (msg.data === "2") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '12:00' AND '14:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);

            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom12To14.push(elements);
                }
              });
            }
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom12To14);
    }

    if (msg.data === "3") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '14:00' AND '16:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);
            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom14To16.push(elements);
                }
              });
            }
            console.log("response: ", response);
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom14To16);
    }

    if (msg.data === "4") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '16:00' AND '18:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);
            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom16To18.push(elements);
                }
              });
            }
            console.log("response: ", response);
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom16To18);
    }

    if (msg.data === "5") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '18:00' AND '20:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);
            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom18To20.push(elements);
                }
              });
            }
            console.log("response: ", response);
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom18To20);
    }

    if (msg.data === "6") {
      try {
        pool
          .query(
            `SELECT DISTINCT
        orders.ID,
        orders.post_status,
        orders.post_type,
        order_name.order_item_name AS 'order_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
        MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
        MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
        MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
        MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '20:00' AND '22:00' THEN postmeta.meta_value END) AS 'time'
                
        FROM
        wp_posts AS orders
        INNER JOIN wp_postmeta AS postmeta
        ON orders.ID = postmeta.post_id 
                
        INNER JOIN wp_woocommerce_order_items AS order_name
        ON orders.ID = order_name.order_id 
                
        WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
        GROUP BY ID`
          )
          .then((response) => {
            let n = response[0].filter((elem) => elem.time !== null);
            if (n.length === 0) {
              BOT.sendMessage(chat_id, "Заказов нет");
            }

            if (n.length > 0) {
              return n.forEach((elements) => {
                if (elements.time !== null) {
                  console.log("elements: ", elements);
                  assemblyCollectionFrom20To22.push(elements);
                }
              });
            }
            console.log("response: ", response);
          })
          .catch((pool_error) => {
            console.log("pool_error: ", pool_error);
          });
      } catch (error) {
        console.error("error: ", error);
      }

      await chooseProductForAssembly(chat_id, assemblyCollectionFrom20To22);
    }
  });
}

async function getProcessingProduct(chat_id) {
  let productCollection = [];

  try {
    await pool
      .query(processingProductQuery)
      .then((response) => {
        console.log("response: ", response);
        if (response[0].length <= 0) {
          return BOT.sendMessage(chat_id, "Заказов нет");
        }
        return response.forEach((elements) => {
          return elements.forEach((elem) => {
            productCollection.push(elem);
          });
        });
      })
      .catch((pool_error) => {
        console.log("pool_error: ", pool_error);
      });
  } catch (error) {
    console.log("error: ", error);
  }

  await chooseProductForManager(chat_id, productCollection);
}

async function userRoles(chat_id, user) {
  let value = Object.values(user);

  if (value[2].includes("shop_manager") === true) {
    // aleksandra.m@filancy.com
    setTimeout(() => {
      return BOT.sendMessage(
        chat_id,
        `Менеджер - ${user.display_name}`,
        managerActions
      );
    }, 1000);

    await BOT.on("message", async (msg) => {
      if (msg.text === "Получить заказы") {
        getProcessingProduct(chat_id);
      }

      if (msg.text === "Статусы") {
        try {
          pool
            .query(countStatusesQuery)
            .then((response) => {
              return response[0].forEach((elem) => {
                setTimeout(() => {
                  BOT.sendMessage(
                    chat_id,
                    `
Отмена - ${elem.cancelled}
Сборка - ${elem.assembly}
Доставка - ${elem.transporting}
Выполнен - ${elem.completed}
                  `
                  );
                }, 1000);
              });
            })
            .catch((e) => {
              console.log("pool_error: ", e);
            });
        } catch (e) {
          console.log("error: ", e);
        }
      }
    });

    console.log("is shop_manager");
  }

  if (value[2].includes("shop_deliver") === true) {
    // mykhailova.alexandra11@gmail.com
    setTimeout(() => {
      return BOT.sendMessage(
        chat_id,
        `Курьер - ${user.display_name}`,
        deliverActions
      );
    }, 1000);

    await BOT.on("message", async (msg) => {
      if (msg.text === "Получить заказы") {
        getAssemblyProduct(chat_id);
      }
    });

    console.log("is shop_deliver");
  }

  if (value[2].includes("shop_storekeeper") === true) {
    // uhmsnic@gmail.com
    setTimeout(() => {
      return BOT.sendMessage(
        chat_id,
        `Кладовщик - ${user.display_name}`,
        storekeeperActions
      );
    }, 1000);

    await BOT.on("message", async (msg) => {
      if (msg.text === "Получить заказы") {
        getTransportingProduct(chat_id);
      }
    });

    console.log("is shop_storekeeper");
  }

  console.log("chat_id: ", chat_id);
}

async function checkUserByEmail(chat_id) {
  let emailsCollection = [];

  await pool
    .query(emailQuery)
    .then((response) => {
      return response.forEach((element) => {
        return element.forEach((elem) => {
          return emailsCollection.push(elem);
        });
      });
    })
    .catch((error) => {
      console.log("error: ", error);
    });

  await BOT.once("message", async (msg) => {
    const user_text = msg.text;

    let userEmail = emailsCollection.find((elem) => elem.user_email === user_text) || {};

    console.log("userEmail: ", userEmail);

    return userEmail?.user_email === undefined ? BOT.sendMessage(chat_id, `Такого пользователя не существует`) : userRoles(chat_id, userEmail);
  });
}

const start = () => {
  BOT.on("message", async (msg) => {
    let user_text = msg.text;
    let chat_id = msg.chat.id;

    if (user_text === "/start") {
      await setTimeout(() => {
        BOT.sendMessage(chat_id, `Здравствуйте, введите свой email`);
        checkUserByEmail(chat_id);
      }, 1000);
    }
  });
};

start();