const emailQuery = (`SELECT user_email, display_name, role.meta_value AS 'role' FROM wp_users AS users
INNER JOIN wp_usermeta AS usermeta ON users.ID = usermeta.user_id AND usermeta.meta_key = 'wp_capabilities'
INNER JOIN wp_usermeta AS role ON users.ID = role.user_id AND role.meta_key = 'wp_capabilities'`);

const processingProductQuery = (`SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-processing'
GROUP BY ID`);

const assemblyProductQuery = (`SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-assembly'
GROUP BY ID`); 

const transportingProductQuery = (`SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-transporting'
GROUP BY ID`); 

const selectCancelledItemsQuery = (`
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-cancelled'
GROUP BY ID
`);

const countStatusesQuery = (`
SELECT DISTINCT 
COUNT(CASE WHEN post_status = 'wc-cancelled' THEN post_status END) AS 'cancelled',
COUNT(CASE WHEN post_status = 'wc-assembly' THEN post_status END) AS 'assembly',
COUNT(CASE WHEN post_status = 'wc-transporting' THEN post_status END) AS 'transporting',
COUNT(CASE WHEN post_status = 'wc-completed' THEN post_status END) AS 'completed'
FROM wp_posts WHERE post_type = 'shop_order'
`);



module.exports = {
  emailQuery,
  processingProductQuery,
  assemblyProductQuery,
  transportingProductQuery,
  selectCancelledItemsQuery,
  countStatusesQuery
}