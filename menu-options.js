module.exports = {
  menuOptions: {
    reply_markup: JSON.stringify({
      keyboard: [
        [{ text: "Менеджер", callback_data: "manager" }, { text: "Кладовщик", callback_data: "shop_storekeeper" }],
        [{ text: "Курьер", callback_data: "shop_delivery" }],
      ]
    })
  },

  managerActions: {
    reply_markup: JSON.stringify({
      resize_keyboard: true,
      keyboard: [
        [{ text: 'Получить заказы', callback_data: 'get_orders' }, { text: 'Статусы', callback_data: 'statuses' }]
      ]
    })
  },
  deliverActions: {
    reply_markup: JSON.stringify({
      resize_keyboard: true,
      keyboard: [
        [{ text: 'Получить заказы', callback_data: 'get_orders' }]
      ]
    })
  },
  storekeeperActions: {
    reply_markup: JSON.stringify({
      resize_keyboard: true,
      keyboard: [
        [{ text: 'Получить заказы', callback_data: 'get_orders' }]
      ]
    })
  },

  managerChangeAction: {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [{ text: 'Сборка', callback_data: 'change_status' }, { text: 'Отмена', callback_data: 'cancel' }]
      ]
    })
  },

  timeInterval: {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [{ text: '10:00 - 12:00', callback_data: '1'}, { text: '12:00 - 14:00', callback_data: '2'}, { text: '14:00 - 16:00', callback_data: '3'}],
        [{ text: '16:00 - 18:00', callback_data: '4'}, { text: '18:00 - 20:00', callback_data: '5'}, { text: '20:00 - 22:00', callback_data: '6'}]
      ]
    })
  }
}