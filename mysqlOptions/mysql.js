const mysql = require('mysql2');
const dotenv = require('dotenv');
// import dotenv from 'dotenv';
dotenv.config('local');

let connection = null;

try {
  connection = {
    mysql__pool: mysql.createConnection({
      host: 'm3rem.spectrum.myjino.ru',
      user: '939341_wp1',
      password: '93nSn3jRe',
      database: '939341_wp1'
    }).promise()
  }

  connection.mysql__pool.connect()
    .then(() => {
      console.log('connect successfully');
    })
    .catch((connect_error) => {
      console.log(`Something went wrong with connection: ${connect_error}`);
    })

} catch (error) {
  console.log(`Something went wrong ${error}`);
}

module.exports = connection;