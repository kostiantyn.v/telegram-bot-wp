let connection = require('../mysqlOptions/mysql').mysql__pool;
const TELEGRAM_API = require('node-telegram-bot-api');
const { emailQuery } = require('../queryList');
const { Manager } = require('../managerInstance/managerInstance');

class TelegramBot {
  constructor(token) {
    this.client = new TELEGRAM_API(token, { polling: true });
  }

  start() {
    this.client.once('message', async (msg) => {
      let user_text = msg.text;
      let chat_id = msg.chat.id;
      let user_first_name = msg.chat.first_name;

      if (user_text === '/start') {
        console.log('client: ', this.client);
        await setTimeout(() => {
          this.client.sendMessage(chat_id, `Hello, ${user_first_name}`);
          this.checkUserByEmail(chat_id);
        }, 1000);
      }
    });
  }

  checkUserByEmail(chat_id) {
    let emailsCollection = [];

    connection.query(emailQuery)
      .then((response) => {
        return response.forEach(element => {
          return element.forEach(e => {
            console.log('e: ', e);
            return emailsCollection.push(e);
          })
        });
      })
      .catch(error => {
        console.log('error: ', error);
      });

    this.client.once('message', async (msg) => {
      const user_text = msg.text;
      console.log('user text: ', user_text);

      let email = emailsCollection.find(e => e.user_email === user_text);

      // const manager = new Manager(email.user_email, this.client);

      // console.log('manager: ', manager.getProcessingProduct());
      await setTimeout(() => {
        return email === undefined ? this.client.sendMessage(chat_id, `Такого пользователя не существует`) : this.client.sendMessage(chat_id, `Ваш email - ${email.user_email}`);
      }, 1000);
    });
  }
}

module.exports = { TelegramBot }